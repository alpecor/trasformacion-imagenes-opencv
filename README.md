# Trasformacion de imagenes con OpenCv & Python

Programa pensado para usar imágenes de la carpeta */source* y copiarlas a carpetas con distintas modificaciones utilizando la libreria de openCv, mide ademas el tiempo transcurrido en estas operaciones.


---------------
## Requerimientos
-----------------
### En Windows
    + Python 3.6
        + PIP
            + python -m pip install --upgrade pip
            + pip install python-resize-image
        + OPENCV
            + pip install opencv-python
  
### En Ubuntu & Debian

    + Python
        + sudo apt update
        + sudo apt install python3.6
    + OPENCV
        + sudo apt-get install python-pip python-dev build-essential 
        + sudo pip install --upgrade pip 
        + pip install python-resize-image


--------------
## Funciones
-------------
+ fil_medio(origen,destino)
+ fil_Black_Hat(origen,destino)
+ fil_reduce(origen, destino, NuevoAncho,NuevoAlto, calidad)


-----------
## Licencia
---------
Aplicacion de dominio publico.