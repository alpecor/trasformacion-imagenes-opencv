#!/usr/bin/python3
import subprocess
import os
start_path = 'source/'
import cv2
import numpy as np
from PIL import Image
from time import time

def fil_medio(fp,name):
    # read the image
    image = cv2.imread(fp)
    # apply the 3x3 mean filter on the image
    kernel = np.ones((3,3),np.float32)/9
    processed_image = cv2.filter2D(image,-1,kernel)
    # save image to disk
    cv2.imwrite(name, processed_image)


def fil_Black_Hat(fp,name):
    #Cargar la mascara
    imagen = cv2.imread(fp,0)
    #Crear un kernel de '1' de 3x3
    kernel = np.ones((9,9),np.uint8)
    #Se aplica la transformacion: Black Hat
    transformacion = cv2.morphologyEx(imagen,cv2.MORPH_BLACKHAT,kernel)
    #Mostrar el resultado y salir
    cv2.imwrite(name, transformacion)

def fil_reducir(fp,name,nuevoAncho,nuevoAlto,calidad):
    with open(fp, 'r+b') as f:
        with Image.open(f) as image:
            cover = resizeimage.resize_thumbnail(image, [nuevoAncho, nuevoAlto])
            print (name)
            cover.save(name, image.format,quality=calidad)

    
if __name__ == '__main__':
    start_time = time()
    for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                fil_medio(fp,'filtro_medio/'+f)
                fil_Black_Hat(fp,'filtro_black_Hat/'+f)
                fil_reducir(fp,'filtro_reducir/'+f,5472,3648,30)
    elapsed_time = time() - start_time
    print("Triempo transcurrido: %0.10f seconds." % elapsed_time)

